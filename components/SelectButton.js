import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const SelectButton = props => {
    return(
        <TouchableOpacity activeOpacity={0.6} onPress={props.onPress}>
            <View style={[styles.buttonContainer, {backgroundColor: props.color} ]}>
                <Text style={styles.buttonText}>{props.children}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    buttonContainer: {
        paddingHorizontal: 25,
        paddingVertical: 12,
        borderRadius: 20
    },
    buttonText: {
        color: 'white',
        fontFamily: 'open-sans',
        fontSize: 14
    }
})

export default SelectButton;