import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import BodyText from '../components/BodyText';
import MainButton from '../components/MainButton';
import TitleText from '../components/TitleText';
import colors from '../constants/colors';

const GameOverScreen = props => {
    return (
        <View style={styles.screen}>
            <TitleText>The Game is Over</TitleText>
            <View style={styles.imageContainer}>
                <Image
                    source={require('../assets/success.png')}
                    style={styles.image}
                    resizeMode="cover"
                />
            </View>
            <View style={styles.resultContainer}>
                <BodyText style={styles.resultText}>
                    Your phone needed <Text style={styles.highlight}>{props.numberRounds}</Text> to guess the number
                    <Text style={styles.highlight}>{props.userChoice}</Text>
                </BodyText>
            </View>
            <MainButton title="NEW GAME" onPress={props.onRestart}/>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageContainer: {
        borderWidth: 3,
        borderColor: 'black',
        width: 300,
        height: 300,
        borderRadius: 150,
        overflow: 'hidden',
        marginVertical: 30
    },
    image: {
        width: '100%',
        height: '100%',
    },
    highlight: {
        color: colors.primary
    },
    resultContainer: {
        marginHorizontal: 20,
        marginVertical: 20
    },
    resultText: {
        fontSize: 20,
        textAlign: 'center'
    }
})

export default GameOverScreen;

